import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { NgxErrorsModule } from '@ultimate/ngxerrors';


import { ProfilePage } from '../pages/profile/profile';
import { OtherPage } from '../pages/other/other';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { DetailArtikelPage } from '../pages/detail-artikel/detail-artikel';
import { MedicinePage } from '../pages/medicine/medicine';
import { HospitalPage } from '../pages/hospital/hospital';
import { MedicalPage } from '../pages/medical/medical';
import { SchedulePage } from '../pages/schedule/schedule';
import { ReminderPage } from '../pages/reminder/reminder';
import { HelpPage } from '../pages/help/help';
import { HistoryPage } from '../pages/history/history';
import { RegisterPage } from '../pages/register/register'
import { SignupPage } from '../pages/signup/signup';
import { WelcomePage } from '../pages/welcome/welcome';
import { BawangputihPage } from '../pages/bawangputih/bawangputih';
import { KurangtidurPage } from '../pages/kurangtidur/kurangtidur';
import { UmurpanjangPage } from '../pages/umurpanjang/umurpanjang';
import { PencernaanPage } from '../pages/pencernaan/pencernaan';
import { AsamuratPage } from '../pages/asamurat/asamurat';
import { RsumumPage } from '../pages/rsumum/rsumum';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


//import database firebaseconfig
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../services/auth.service';
import { firebaseconfig } from '../config';
import { from } from 'rxjs/observable/from';

@NgModule({
  declarations: [
    MyApp,
    ProfilePage,
    OtherPage,
    HomePage,
    TabsPage,
    LoginPage,
    MedicinePage,
    HospitalPage,
    MedicalPage,
    SchedulePage,
    ReminderPage,
    HelpPage,
    HistoryPage,
    RegisterPage,
    DetailArtikelPage,
    SignupPage,
    WelcomePage,
    BawangputihPage,
    KurangtidurPage,
    UmurpanjangPage,
    PencernaanPage,
    RsumumPage,
    AsamuratPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseconfig),
    NgxErrorsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProfilePage,
    OtherPage,
    HomePage,
    TabsPage,
    LoginPage,
    MedicinePage,
    HospitalPage,
    MedicalPage,
    SchedulePage,
    ReminderPage,
    HelpPage,
    HistoryPage,
    RegisterPage,
    DetailArtikelPage,
    SignupPage,
    WelcomePage,
    BawangputihPage,
    KurangtidurPage,
    UmurpanjangPage,
    PencernaanPage,
    RsumumPage,
    AsamuratPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthService,
    AngularFireAuth
  ]
})
export class AppModule { }
