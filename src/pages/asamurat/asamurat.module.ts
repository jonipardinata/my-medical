import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AsamuratPage } from './asamurat';

@NgModule({
  declarations: [
    AsamuratPage,
  ],
  imports: [
    IonicPageModule.forChild(AsamuratPage),
  ],
})
export class AsamuratPageModule {}
