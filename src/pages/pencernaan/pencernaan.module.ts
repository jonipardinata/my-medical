import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PencernaanPage } from './pencernaan';

@NgModule({
  declarations: [
    PencernaanPage,
  ],
  imports: [
    IonicPageModule.forChild(PencernaanPage),
  ],
})
export class PencernaanPageModule {}
