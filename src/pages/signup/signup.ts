import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { TabsPage } from '../tabs/tabs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';



/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  signupError: string;
  form: FormGroup;

  constructor(public toast: ToastController, public auth: AuthService, public fb: FormBuilder, public navCtrl: NavController, public navParams: NavParams) {
    this.form = fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(5)])]
    })
  }

  signup() {
    let data = this.form.value;
    let credentials = {
      email: data.email,
      password: data.password
    };

    this.auth.signUp(credentials).then(
      () => this.navCtrl.setRoot(LoginPage),
      error => this.toast.create({
        message: error.message,
        duration: 5000,
        position: 'top'
      }).present()
    )
  }

  goToSigninPage() {
    this.navCtrl.push(LoginPage);
  }

}
