import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MedicinePage } from '../medicine/medicine';
import { HospitalPage } from '../hospital/hospital';
import { SchedulePage } from '../schedule/schedule';
import { MedicalPage } from '../medical/medical';
import { HelpPageModule } from '../help/help.module';
import { from } from 'rxjs/observable/from';

/**
 * Generated class for the HelpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-help',
  templateUrl: 'help.html',
})
export class HelpPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpPage');
  }
  gotomedical() {
    this.navCtrl.push(MedicalPage);
  }
  gotohospital() {
    this.navCtrl.push(HospitalPage);
  }
  gotoschedule() {
    this.navCtrl.push(SchedulePage);
  }
  gotomedicine() {
    this.navCtrl.push(MedicinePage);
  }
  gotohelp() {
    this.navCtrl.push(HelpPage);
  }

}
