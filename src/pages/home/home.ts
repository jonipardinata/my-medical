import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DetailArtikelPage } from '../detail-artikel/detail-artikel'
import { BawangputihPage } from '../bawangputih/bawangputih';
import { KurangtidurPage } from '../kurangtidur/kurangtidur';
import { PencernaanPage } from '../pencernaan/pencernaan';
import { AsamuratPage } from '../asamurat/asamurat';
import { UmurpanjangPage } from '../umurpanjang/umurpanjang'
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  data = [
    'satu',
    'dua',
    'tiga'
  ];
  datas;

  constructor(public navCtrl: NavController) {

  }

  ionViewDidLoad() {
    console.log(this.data);
    this.datas = this.data['data'];
  }

  gotoDetailArtikel(){
    this.navCtrl.push(DetailArtikelPage);
  }

   
  gotokurangtidur() {
    this.navCtrl.push(KurangtidurPage);
  }
  gotopencernaan() {
    this.navCtrl.push(PencernaanPage);
  }
  gotoasamurat() {
    this.navCtrl.push(AsamuratPage);
  }
  gotoumurpanjang() {
    this.navCtrl.push(UmurpanjangPage);
  }
}
