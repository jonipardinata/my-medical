import { NgModule, Directive } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';


// @Directive({
//   exportAs: NgxErrorsModule
// })
@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
  ],
})
export class LoginPageModule { }
