import { Component, Directive } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { SignupPage } from '../signup/signup';
import { TabsPage } from '../tabs/tabs';

import { FormGroup, FormBuilder, Validator, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginForm: FormGroup;
  loginError: string;

  constructor(public toast: ToastController, public navCtrl: NavController, public navParams: NavParams, private auth: AuthService, private fb: FormBuilder) {

    this.loginForm = fb.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(5)])]
    })

  }

  gotoRegister() {
    this.navCtrl.push(RegisterPage);
  }

  goToSignup() {
    this.navCtrl.push(SignupPage);
  }

  login() {
    let data = this.loginForm.value;

    if (!data.email) {
      return;
    }

    let credential = {
      email: data.email,
      password: data.password
    };

    this.auth.signInWithEmail(credential).then(
      () => this.navCtrl.setRoot(TabsPage),
      error => this.toast.create({
        message: error.message,
        duration: 5000,
        position: 'top'
      }).present()
    );




  }
}
