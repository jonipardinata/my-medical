import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RsumumPage } from './rsumum';

@NgModule({
  declarations: [
    RsumumPage,
  ],
  imports: [
    IonicPageModule.forChild(RsumumPage),
  ],
})
export class RsumumPageModule {}
