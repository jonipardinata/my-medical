import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MedicinePage } from '../medicine/medicine';
import { HospitalPage } from '../hospital/hospital';
import { MedicalPage } from '../medical/medical';
import { SchedulePage } from '../schedule/schedule';
import { ReminderPage } from '../reminder/reminder';
import { HelpPage } from '../help/help';

/**
 * Generated class for the OtherPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-other',
  templateUrl: 'other.html',
})
export class OtherPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  gotoMedicine() {
    this.navCtrl.push(MedicinePage);
  }
  gotoHospital() {
    this.navCtrl.push(HospitalPage);
  }
  gotoMedical() {
    this.navCtrl.push(MedicalPage);
  }
  gotoSchedule() {
    this.navCtrl.push(SchedulePage);
  }
  gotoReminder() {
    this.navCtrl.push(ReminderPage);
  }
  gotoHelp() {
    this.navCtrl.push(HelpPage);
  }

}
