import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BawangputihPage } from './bawangputih';

@NgModule({
  declarations: [
    BawangputihPage,
  ],
  imports: [
    IonicPageModule.forChild(BawangputihPage),
  ],
})
export class BawangputihPageModule {}
