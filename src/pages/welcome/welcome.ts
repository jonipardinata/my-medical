import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';

import { AuthService } from '../../services/auth.service';

/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthService) {
    this.auth.afAuth.authState
      .subscribe(
        user => {
          if (user) {
            return;
          } else {
            this.navCtrl.setRoot(LoginPage)
          }
        },
        () => {
          this.navCtrl.setRoot(LoginPage);
        }
      )

  }



  login() {
    this.auth.signOut();
    this.navCtrl.setRoot(LoginPage);
  }

  navigateToLogin() {
    this.navCtrl.push(LoginPage);
  }

}
