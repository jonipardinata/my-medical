import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KurangtidurPage } from './kurangtidur';

@NgModule({
  declarations: [
    KurangtidurPage,
  ],
  imports: [
    IonicPageModule.forChild(KurangtidurPage),
  ],
})
export class KurangtidurPageModule {}
