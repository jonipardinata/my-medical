import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RsumumPage } from '../rsumum/rsumum'; 
import { from } from 'rxjs/observable/from';

/**
 * Generated class for the HospitalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hospital',
  templateUrl: 'hospital.html',
})
export class HospitalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  gotorsumum() {
    this.navCtrl.push(RsumumPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad HospitalPage');
  }

}
