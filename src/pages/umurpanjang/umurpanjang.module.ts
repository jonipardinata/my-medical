import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UmurpanjangPage } from './umurpanjang';

@NgModule({
  declarations: [
    UmurpanjangPage,
  ],
  imports: [
    IonicPageModule.forChild(UmurpanjangPage),
  ],
})
export class UmurpanjangPageModule {}
